using System.Collections.Generic;
using GameReady.Analytics.Data;
using GameReady.EditorTools;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Gameready.Analytics.Editor
{
    [CustomEditor(typeof(BaseAnalyticKeys))]
    public class BaseAnalyticKeysEditor : UnityEditor.Editor
    {
        private BaseAnalyticKeys _eventsMap;

        private void OnEnable()
        {
            _eventsMap = (BaseAnalyticKeys)target;
        }

        public override VisualElement CreateInspectorGUI()
        {
            var container = new VisualElement();

            InspectorElement.FillDefaultInspector(container, serializedObject, CreateEditor(target));

            var generateConsts = new Button(GenerateConstantsClass);
            generateConsts.text = "Generate Event Constants Class";
            container.Add(generateConsts);

            return container;
        }

        private void GenerateConstantsClass()
        {
            var property = serializedObject.FindProperty("_eventKeys");
            var list = new List<string>();
            for (int i = 0; i < property.arraySize; i++)
            {
                var element = property.GetArrayElementAtIndex(i);
                list.Add(element.FindPropertyRelative("key").stringValue);
            }

            ConstClassGenerator.GenerateConstClassFile("Saves AnalyticConstants", "AnalyticEventKeys",
                "AnalyticEventKeys", list.ToArray());
        }
    }
}