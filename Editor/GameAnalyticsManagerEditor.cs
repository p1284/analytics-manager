using System;
using GameReady.Analytics;
using GameReady.Analytics.Core;
using GameReady.Analytics.Data;
using GameReady.Analytics.Proividers;
using GameReady.EditorTools;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Gameready.Analytics.Editor
{
    [CustomEditor(typeof(GameAnalyticsConfig), true)]
    public class GameAnalyticsManagerEditor : UnityEditor.Editor
    {
        private GameAnalyticsConfig _analyticsConfig;

        private void OnEnable()
        {
            _analyticsConfig = (GameAnalyticsConfig)target;
        }

        private bool HasProvider<T>(SerializedProperty property) where T : BaseAnalyticProvider
        {
            foreach (SerializedProperty element in property)
            {
                if (element.objectReferenceValue != null && element.objectReferenceValue is T)
                {
                    return true;
                }
            }

            return false;
        }

        public override VisualElement CreateInspectorGUI()
        {
            var container = new VisualElement();

            InspectorElement.FillDefaultInspector(container, serializedObject, CreateEditor(target));

            var providers = serializedObject.FindProperty("_providers");


#if UNITY_ANALYTICS
            var btn = new Button(() =>
            {
                EditorHelpers.RemoveDefineForIOSAndroid("UNITY_ANALYTICS");
                EditorHelpers.UninstallPackage("com.unity.services.analytics", result => { });
            });


            btn.text = "Remove UnityAnalytics";
            container.Add(btn);

            if (!HasProvider<UnityAnalyticsProvider>(providers))
            {
                var btnAddProvider = new Button(() =>
                {
                    providers.arraySize++;
                    var element = providers.GetArrayElementAtIndex(providers.arraySize - 1);
                    element.objectReferenceValue =
                        EditorHelpers.CreateScriptableAsset<UnityAnalyticsProvider>(false, true);
                    element.serializedObject.ApplyModifiedProperties();
                });
                btnAddProvider.text = "Create UnityAnalytics Provider";
                container.Add(btnAddProvider);
            }

#else
            var button = new Button(() =>
            {
                EditorHelpers.InstallPackage("com.unity.services.analytics",
                    result => { EditorHelpers.AddDefineForCurrentPlatform("UNITY_ANALYTICS"); });
            });
            button.text = "Setup UnityAnalytics";
            container.Add(button);

#endif

            var eventsMapButton = new Button(() =>
            {
                EditorHelpers.CreateScriptableAsset<BaseAnalyticKeys>(true, true);
            });
            eventsMapButton.text = "Create Base Events Map";
            container.Add(eventsMapButton);

            var validateBtn = new Button(() => _analyticsConfig.Validate());
            validateBtn.text = "Validate";
            container.Add(validateBtn);

            return container;
        }
    }
}