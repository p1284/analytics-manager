using GameReady.Analytics.Data;
using GameReady.EditorTools;
using UnityEditor;

namespace Gameready.Analytics.Editor
{
    public static class AnalyticsSetupHelper
    {

        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Analytics/GetOrCreate Game Analytics Config", false, 1)]
        public static void CreateAnalyticsManager()
        {
            EditorHelpers.CreateScriptableAsset<GameAnalyticsConfig>(true, true);
        }

#if CHEATS
        [MenuItem(EditorHelpers.DEFAULT_MENU_ITEMNAME + "/Analytics/Create AnalyticsCheats", false, 0)]
        public static void CreateAnalyticsCheats()
        {
            EditorHelpers.CreateScriptableAsset<Gameready.Analytics.Cheats.AnalyticsCheats>(true, true);
        }
#endif
    }
}