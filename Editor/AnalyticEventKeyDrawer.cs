using GameReady.Analytics.Data;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Gameready.Analytics.Editor
{
    [CustomPropertyDrawer(typeof(AnalyticEventKey))]
    public class AnalyticEventKeyDrawer : PropertyDrawer
    {
        private SerializedProperty _property;

        private DropdownField _eventsDropDown;

        private SerializedProperty _eventKeyProperty;

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new VisualElement();

            var hbox = new VisualElement();

            hbox.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
            hbox.style.flexGrow = 1f;
            hbox.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);
            //hbox.style.alignItems = new StyleEnum<Align>(Align.Stretch);
            container.Add(hbox);

            _property = property;
            _eventKeyProperty = _property.FindPropertyRelative("EventName");

            if (BaseAnalyticKeys.EventsMapEditorOnly != null)
            {
                _eventsDropDown = new DropdownField(property.displayName);
                _eventsDropDown.style.width = new StyleLength(Length.Percent(70));
                _eventsDropDown.style.flexShrink = 0f;
                _eventsDropDown.choices = BaseAnalyticKeys.EventsMapEditorOnly;
                _eventsDropDown.RegisterValueChangedCallback(OnEventChangedHandler);
                _eventsDropDown.value = _eventKeyProperty.stringValue;
                hbox.Add(_eventsDropDown);

                var filterFiled = new PropertyField(property.FindPropertyRelative("Filter"));
                filterFiled.label = "";
                filterFiled.style.width = new StyleLength(Length.Percent(30));
                filterFiled.style.flexShrink = 0f;
                hbox.Add(filterFiled);

                container.Add(hbox);
            }
            else
            {
            }

            return container;
        }

        private void OnEventChangedHandler(ChangeEvent<string> evt)
        {
            _eventKeyProperty.stringValue = evt.newValue;
            _eventKeyProperty.serializedObject.ApplyModifiedProperties();
        }
    }
}