using System;
using GameReady.Analytics;
using GameReady.Analytics.Core;
using GameReady.Analytics.Data;
using GameReady.Analytics.Proividers;
using GameReady.EditorTools;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Gameready.Analytics.Editor
{
    [CustomEditor(typeof(BaseAnalyticProvider), true)]
    public class BaseProviderEditor : UnityEditor.Editor
    {
        private BaseAnalyticProvider _provider;

        private void OnEnable()
        {
            _provider = (BaseAnalyticProvider)target;
        }


        public override VisualElement CreateInspectorGUI()
        {
            var container = new VisualElement();

            var backButton = new Button(() => { EditorHelpers.SelectAssetByType<GameAnalyticsConfig>(); });
            backButton.text = "<--";
            backButton.style.width = new StyleLength(Length.Percent(10));
            container.Add(backButton);

            InspectorElement.FillDefaultInspector(container, serializedObject, CreateEditor(target));

            UnityAnalyticsProviderOptions(container);

            return container;
        }

        private void UnityAnalyticsProviderOptions(VisualElement container)
        {
            if (_provider is UnityAnalyticsProvider)
            {
#if UNITY_ANALYTICS_EVENT_LOGS == false
                var btn = new Button(() =>
                {
                    EditorHelpers.AddDefineForCurrentPlatform("UNITY_ANALYTICS_EVENT_LOGS");
                });
                btn.text = "Enable UNITY_ANALYTICS_EVENT_LOGS";
                container.Add(btn);
#else
                var btn = new Button(() =>
                {
                    EditorHelpers.RemoveDefineForCurrentPlatform("UNITY_ANALYTICS_EVENT_LOGS");
                });
                btn.text = "Disable UNITY_ANALYTICS_EVENT_LOGS";
                container.Add(btn);
#endif
            }
        }
    }
}