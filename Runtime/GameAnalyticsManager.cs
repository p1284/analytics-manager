﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameReady.Analytics.Core;
using GameReady.Analytics.Data;
using Gameready.Utils;
using UnityEngine;
using VContainer.Unity;

namespace GameReady.Analytics
{
    public sealed class GameAnalyticsManager : IAnalyticsManager, IDisposable, IAsyncStartable
    {
        public event Action OnInitialized;

        private readonly GameAnalyticsConfig _config;


        public GameAnalyticsManager(GameAnalyticsConfig config)
        {
            _config = config;
        }


        public bool IsInitialized { get; private set; }

        public AnalyticsEventsTracker CreateTracker(string eventKey)
        {
            return new AnalyticsEventsTracker(eventKey, this);
        }

        public bool HasAnalyticProvider(AnalyticTrackFilter filterType)
        {
            foreach (var configProvider in _config.Providers)
            {
                if (configProvider.IsMatch(filterType))
                {
                    return true;
                }
            }

            return false;
        }

        public void TrackCustomEvent(string eventKey,
            Dictionary<string, object> parameters = null,
            AnalyticTrackFilter filterType = AnalyticTrackFilter.All)
        {
            if (Application.isEditor && _config.EnabledOnBuildsOnly)
            {
                if (_config.LOGTrackedEvents && Debug.isDebugBuild)
                {
                    this.Log($"TrackEvent:{eventKey} only for Builds", filterType);
                }

                return;
            }

            TrackCustomEventInternal(new AnalyticEventParams(eventKey, filterType, parameters));
        }

        public void TrackAdsImpression(AnalyticTrackFilter filter, AdsImpressionAnalyticData data)
        {
            if (Application.isEditor && _config.EnabledOnBuildsOnly)
            {
                if (_config.LOGTrackedEvents && Debug.isDebugBuild)
                {
                    this.Log($"TrackAdsImpression:{data} only for Builds", filter);
                }

                return;
            }

            if (data == null) return;
            var isMathed = false;
            foreach (var provider in _config.Providers)
            {
                if (provider != null && provider.IsMatch(filter))
                {
                    isMathed = true;
                    (provider as ITrackAdsImpression)?.TrackAdsImpression(data);
                }
            }

            if (isMathed && _config.LOGTrackedEvents && Debug.isDebugBuild)
            {
                this.Log($"TrackAdsImpression:{data}", filter);
            }
        }

        public void ForceTrackEvents(AnalyticTrackFilter filterType)
        {
            if (Application.isEditor && _config.EnabledOnBuildsOnly) return;
            foreach (var provider in _config.Providers)
            {
                if (provider != null)
                {
                    provider.ForceTrackEvents(filterType);
                }
            }
        }

        private void TrackCustomEventInternal(AnalyticEventParams eventData)
        {
            foreach (var provider in _config.Providers)
            {
                if (provider != null)
                {
                    provider.TrackEventInternal(eventData);
                }
            }

            if (_config.LOGTrackedEvents && Debug.isDebugBuild)
            {
                this.Log($"TrackEvent:{eventData.ToString()}", eventData.TrackFilter);
            }
        }

        public T GetAnalyticProvider<T>() where T : BaseAnalyticProvider
        {
            foreach (var provider in _config.Providers)
            {
                if (provider is T typeProvider)
                {
                    return typeProvider;
                }
            }

            return default(T);
        }


        public void Dispose()
        {
            OnInitialized.RemoveAll();
            foreach (var provider in _config.Providers)
            {
                provider.Dispose();
            }
        }

        public async UniTask StartAsync(CancellationToken cancellation)
        {
            foreach (var configProvider in _config.Providers)
            {
                if (configProvider == null) continue;
                configProvider.Validate();
                await configProvider.InitializeAsync();
            }

            await UniTask.Yield(PlayerLoopTiming.LastPostLateUpdate);

            this.Log("Initialized");
            IsInitialized = true;
        }
    }
}