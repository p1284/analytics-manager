using GameReady.Analytics.Data;

namespace GameReady.Analytics.Core
{
    public interface ITrackAdsImpression
    {
        void TrackAdsImpression(AdsImpressionAnalyticData data);
    }
}