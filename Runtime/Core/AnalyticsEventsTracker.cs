using System.Collections.Generic;
using GameReady.Analytics.Data;

namespace GameReady.Analytics.Core
{
    public class AnalyticsEventsTracker
    {
        private string _eventKey;
        private readonly GameAnalyticsManager _gameAnalyticsManager;

        private Dictionary<string, object> _parameters;

        public AnalyticsEventsTracker(string eventKey, GameAnalyticsManager gameAnalyticsManager)
        {
            _eventKey = eventKey;
            _gameAnalyticsManager = gameAnalyticsManager;
        }

        public AnalyticsEventsTracker SetEventKey(string eventKey)
        {
            _eventKey = eventKey;
            return this;
        }

        public AnalyticsEventsTracker ClearParams()
        {
            _parameters?.Clear();
            return this;
        }

        public AnalyticsEventsTracker SetParamValue(string paramName, string value)
        {
            _parameters ??= new Dictionary<string, object>();
            if (!_parameters.TryAdd(paramName, value))
            {
                _parameters[paramName] = value;
            }

            return this;
        }

        public void TrackEvents(AnalyticTrackFilter trackFilter, bool sendImmediate = false)
        {
            _gameAnalyticsManager.TrackCustomEvent(_eventKey, _parameters, trackFilter);
            if (sendImmediate)
            {
                _gameAnalyticsManager.ForceTrackEvents(trackFilter);
            }
        }

        public void TrackAdsImpression(AdsImpressionAnalyticData data, AnalyticTrackFilter trackFilter)
        {
            _gameAnalyticsManager.TrackAdsImpression(trackFilter, data);
        }
    }
}