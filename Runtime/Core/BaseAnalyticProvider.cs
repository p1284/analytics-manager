﻿using System;
using Cysharp.Threading.Tasks;
using GameReady.Analytics.Data;
using UnityEngine;

namespace GameReady.Analytics.Core
{
    public abstract class BaseAnalyticProvider : ScriptableObject,IDisposable
    {
        [Tooltip("You can override BaseAnalyticKeys class")]
        [SerializeField] private BaseAnalyticKeys _eventsMap;

        public abstract UniTask InitializeAsync();

        internal abstract void TrackEventInternal(AnalyticEventParams data);

        protected abstract AnalyticTrackFilter FilterType { get; }

        /// <summary>
        /// Track all events when BaseAnalyticKeys null
        /// otherwise check by BaseAnalyticKeys events and filter
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="eventKey"></param>
        /// <returns></returns>
        protected bool CanTrackEvent(AnalyticTrackFilter filter, string eventKey)
        {
            return _eventsMap == null || _eventsMap.CanTrackThisEvent(eventKey, filter);
        }

        public bool IsMatch(AnalyticTrackFilter filter)
        {
            return (filter & FilterType) > 0;
        }

        public void ForceTrackEvents(AnalyticTrackFilter filter)
        {
            ForceTruckEvents(filter);
        }

        protected virtual void ForceTruckEvents(AnalyticTrackFilter filter)
        {
            Log("Force Track Events");
        }

        internal virtual void Validate()
        {
            Log("Validation complete.");
        }

        protected void Log(string message)
        {
            Debug.Log($"<b>[{GetType().Name}]</b> {message}");
        }

        public abstract void Dispose();
    }
}