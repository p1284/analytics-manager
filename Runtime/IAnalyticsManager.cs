﻿using System;
using System.Collections.Generic;
using GameReady.Analytics.Core;
using GameReady.Analytics.Data;

namespace GameReady.Analytics
{
    public interface IAnalyticsManager
    {
        event Action OnInitialized;
        
        bool IsInitialized { get; }
        
        AnalyticsEventsTracker CreateTracker(string eventKey);

        T GetAnalyticProvider<T>() where T : BaseAnalyticProvider;
        
        void TrackCustomEvent(string eventKey,
            Dictionary<string, object> parameters = null,
            AnalyticTrackFilter filterType = AnalyticTrackFilter.UnityAnalytics);

        void TrackAdsImpression(AnalyticTrackFilter filter, AdsImpressionAnalyticData data);

        void ForceTrackEvents(AnalyticTrackFilter filterType);

        bool HasAnalyticProvider(AnalyticTrackFilter filterType);
    }
}