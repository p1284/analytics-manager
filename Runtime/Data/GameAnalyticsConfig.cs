using GameReady.Analytics.Core;
using Gameready.Utils.Services.Core;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace GameReady.Analytics.Data
{
    public class GameAnalyticsConfig : BaseServiceConfig
    {
        [SerializeField] private BaseAnalyticProvider[] _providers;

        [SerializeField] private bool _logTrackedEvents;

        [Tooltip("Disable track event in editor mode")] [SerializeField]
        private bool _enabledOnBuildsOnly;

        public BaseAnalyticProvider[] Providers => _providers;

        public bool LOGTrackedEvents => _logTrackedEvents;

        public bool EnabledOnBuildsOnly => _enabledOnBuildsOnly;

        public void Validate()
        {
            foreach (var analyticProvider in _providers)
            {
                analyticProvider.Validate();
            }
        }


        public override void RegisterService(IContainerBuilder builder)
        {
            builder.RegisterEntryPoint<GameAnalyticsManager>().As<IAnalyticsManager>().WithParameter(this);
        }
    }
}