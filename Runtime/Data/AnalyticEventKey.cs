using System;
using UnityEngine.Serialization;

namespace GameReady.Analytics.Data
{
    [Serializable]
    public class AnalyticEventKey
    {
        public string EventName;
        public AnalyticTrackFilter Filter;
    }
}