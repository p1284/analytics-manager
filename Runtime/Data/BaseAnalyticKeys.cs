using System.Collections.Generic;
using Gameready.Utils.Data;
using UnityEngine;

namespace GameReady.Analytics.Data
{
    public class BaseAnalyticKeys : ScriptableObject
    {
        [SerializeField] protected KeyValue<string, AnalyticTrackFilter>[] _eventKeys;
        
        public virtual bool CanTrackThisEvent(string eventKey, AnalyticTrackFilter filter)
        {
            foreach (var kv in _eventKeys)
            {
                if (eventKey == kv.key && (filter & kv.value) > 0)
                {
                    return true;
                }
            }

            return false;
        }


#if UNITY_EDITOR

        public static List<string> EventsMapEditorOnly;

        private void OnValidate()
        {
            EventsMapEditorOnly ??= new List<string>();
            EventsMapEditorOnly.Clear();
            foreach (var kv in _eventKeys)
            {
                EventsMapEditorOnly.Add(kv.key);
            }
        }

#endif
    }
}