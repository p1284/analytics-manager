using System;
using System.Collections.Generic;
using System.Text;

namespace GameReady.Analytics.Data
{
    [Flags]
    public enum AnalyticTrackFilter
    {
        None = 0,
        UnityAnalytics = 1,
       // Appmetrica = 2,
        All = UnityAnalytics
    }

    public readonly struct AnalyticEventParams
    {
        public readonly string EventKey;
        public readonly Dictionary<string, object> Parameters;

        public readonly AnalyticTrackFilter TrackFilter;

        public AnalyticEventParams(string eventKey, AnalyticTrackFilter filter,
            Dictionary<string, object> parameters = null)
        {
            EventKey = eventKey;
            TrackFilter = filter;
            Parameters = parameters != null ? new Dictionary<string, object>(parameters) : null;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine($"event_key:{EventKey}");
            if (Parameters != null)
            {
                foreach (var parameter in Parameters)
                {
                    sb.AppendLine($"key:{parameter.Key}-value:{parameter.Value}");
                }
            }

            return sb.ToString();
        }
    }
}