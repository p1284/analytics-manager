namespace GameReady.Analytics.Data
{
    public class AdsImpressionAnalyticData
    {
        public string AdProvider;
        public readonly string Placement;
        public string AdSdk;
        public string AdSdkVersion;
        public string AdUnit;
        public double Revenue;
        public string Currency;
        public string CountryCode;
        public string DspName;
        public string NetworkName;
        /// <summary>
        /// Rewards,Interstitial,Banner
        /// </summary>
        public readonly string AdsType;
        public readonly string Result;

        public AdsImpressionAnalyticData(string adsType, string result, string placement)
        {
            Placement = placement;
            Result = result;
            AdsType = adsType;
        }

        public override string ToString()
        {
            return
                $"type:{AdsType} adProvider:{AdProvider} revenue:{Revenue:f2} placement:{Placement}";
        }
    }
}