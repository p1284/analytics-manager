#if CHEATS
using GameReady.Analytics;
using Gameready.Cheats;
#endif

using UnityEngine;
using UnityEngine.UIElements;
using VContainer;

namespace Gameready.Analytics.Cheats
{
#if CHEATS
    public class AnalyticsCheats : BaseUIToolkitCheat
    {
        private VisualElement _mainContainer;

        private Button _sendDebugEventButton;

        [SerializeField] private string _debugEventName = "debug";

        private IAnalyticsManager _analyticsManager;
        
        protected override VisualElement BuildInternal()
        {
            _analyticsManager = Resolver.Resolve<IAnalyticsManager>();
            
            _mainContainer = _template.Instantiate();

            _sendDebugEventButton = _mainContainer.Q<Button>("debug-event");
            _sendDebugEventButton.clicked += OnSendDebugEventClicked;

            return _mainContainer;
        }

        private void OnSendDebugEventClicked()
        {
            _analyticsManager.TrackCustomEvent(_debugEventName);
        }

        public override void Dispose()
        {
            base.Dispose();
            if (_sendDebugEventButton != null)
                _sendDebugEventButton.clicked -= OnSendDebugEventClicked;

            _mainContainer?.RemoveFromHierarchy();
        }
    }
#endif
}