using System;
using Cysharp.Threading.Tasks;
using GameReady.Analytics.Core;
using GameReady.Analytics.Data;
#if UNITY_ANALYTICS
using Unity.Services.Analytics;
using Unity.Services.Core;
#endif
using UnityEngine;

namespace GameReady.Analytics.Proividers
{
    public class UnityAnalyticsProvider : BaseAnalyticProvider, ITrackAdsImpression
    {
        [SerializeField] private bool _analyticsEnabled = true;

        public override async UniTask InitializeAsync()
        {
            await UniTask.Yield();
#if UNITY_ANALYTICS


            await UnityServices.InitializeAsync();
            await AnalyticsService.Instance.SetAnalyticsEnabled(_analyticsEnabled);
            if (_analyticsEnabled)
            {
                var consents = await AnalyticsService.Instance.CheckForRequiredConsents();

                Log($"Started UGS Analytics Sample with user ID: {AnalyticsService.Instance.GetAnalyticsUserID()}");
            }

#endif
        }


        internal override void TrackEventInternal(AnalyticEventParams data)
        {
#if UNITY_ANALYTICS
            if (CanTrackEvent(data.TrackFilter, data.EventKey))
            {
                if (data.Parameters != null)
                {
                    AnalyticsService.Instance.CustomData(data.EventKey, data.Parameters);
                }
                else
                {
                    AnalyticsService.Instance.CustomData(data.EventKey);
                }
            }
#endif
        }

        protected override AnalyticTrackFilter FilterType => AnalyticTrackFilter.UnityAnalytics;

        protected override void ForceTruckEvents(AnalyticTrackFilter filter)
        {
#if UNITY_ANALYTICS
            if ((filter & AnalyticTrackFilter.UnityAnalytics) > 0)
            {
                AnalyticsService.Instance.Flush();
            }
#endif
        }

        public override void Dispose()
        {
            
        }

        public void TrackAdsImpression(AdsImpressionAnalyticData data)
        {
#if UNITY_ANALYTICS
            var args = new AdImpressionParameters
            {
                AdCompletionStatus = AdCompletionStatus.Completed,
                AdProvider = GetAddProvider(data.AdProvider),
                PlacementName = data.Placement,
                PlacementID = data.Placement,
                PlacementType = GetPlacementType(data.AdsType),
                AdEcpmUsd = data.Revenue,
                SdkVersion = data.AdSdkVersion,
            };

            AnalyticsService.Instance.AdImpression(args);
#endif
        }
#if UNITY_ANALYTICS
        private AdProvider GetAddProvider(string id)
        {
            if (Enum.TryParse(typeof(AdProvider), id, true, out object type))
            {
                return (AdProvider)type;
            }

            return AdProvider.Other;
        }

        private AdCompletionStatus GetCompleteStatus()
        {
            return AdCompletionStatus.Completed;
        }

        private AdPlacementType GetPlacementType(string adsType)
        {
            switch (adsType)
            {
                case "Rewarded":
                    return AdPlacementType.REWARDED;
                case "Interstitial":
                    return AdPlacementType.INTERSTITIAL;
                case "Banner":
                    return AdPlacementType.BANNER;
                default:
                    return AdPlacementType.OTHER;
            }
        }
#endif
    }
}