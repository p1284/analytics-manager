using GameReady.Analytics.Data;
using UnityEngine;

namespace GameReady.Analytics
{
    internal static class InternalAnalyticsLogger
    {
        internal static void Log(this GameAnalyticsManager gameAnalyticsManager, string message,
            AnalyticTrackFilter trackFilter)
        {
            Debug.Log(
                $"<b>[{gameAnalyticsManager.GetType().Name}]</b> {message}\n for {trackFilter}");
        }
        
        internal static void Log(this GameAnalyticsManager gameAnalyticsManager, string message)
        {
            Debug.Log(
                $"<b>[{gameAnalyticsManager.GetType().Name}]</b> {message}");
        }
        
    }
}