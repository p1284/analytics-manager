# Changelog

## [2.0.0] - 2023-10-09
### dependency to VContainer, for Gameready 3.0.0

## [1.0.0] - 2023-09-12
### First Release pack
### Unity Analytics provider implemented